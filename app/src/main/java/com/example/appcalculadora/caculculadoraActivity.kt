package com.example.appcalculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class caculculadoraActivity : AppCompatActivity() {
    private  lateinit var txtUsuario : TextView
    private lateinit var  txtNum1 : EditText
    private lateinit var txtNum2 : EditText
    private lateinit var  txtResultado : TextView
    private  lateinit var  btnSumar : Button
    private  lateinit var  btnRestar : Button
    private  lateinit var  btnMult : Button
    private  lateinit var  btnDiv : Button
    private  lateinit var btnCerrar : Button
    private  lateinit var btnLimpiar : Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_caculculadora)
        iniciarCompontes()
        eventosClic()

    }

    public fun iniciarCompontes(){
        txtUsuario = findViewById(R.id.txtNombre)
        txtResultado = findViewById(R.id.txtResultado)
        txtNum1  = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        btnSumar = findViewById(R.id.btnMas)
        btnRestar = findViewById(R.id.btnRes)
        btnMult = findViewById(R.id.btnMul)
        btnDiv = findViewById(R.id.btnDiv)
        btnCerrar = findViewById(R.id.btnCerrar)
        btnLimpiar = findViewById(R.id.btnLimpiar)


    }

    public fun eventosClic(){
        btnSumar.setOnClickListener(View.OnClickListener {
            var num1 : Float =0.0f
            var num2 : Float =0.0f
            var res  : Float =0.0f

            num1 = txtNum1.text.toString().toFloat()
            num2 = txtNum2.text.toString().toFloat()
            val operaciones  =  Operaciones(num1,num2)
            res = operaciones.suma()
            txtResultado.text = res.toString()




        })

        btnRestar.setOnClickListener(View.OnClickListener {

            var num1 : Float =0.0f
            var num2 : Float =0.0f
            var res  : Float =0.0f

            num1 = txtNum1.text.toString().toFloat()
            num2 = txtNum2.text.toString().toFloat()
            val operaciones  =  Operaciones(num1,num2)
            res = operaciones.resta()
            txtResultado.text = res.toString()



        })

       btnMult.setOnClickListener(View.OnClickListener {
           var num1 : Float =0.0f
           var num2 : Float =0.0f
           var res  : Float =0.0f

           num1 = txtNum1.text.toString().toFloat()
           num2 = txtNum2.text.toString().toFloat()
           val operaciones  =  Operaciones(num1,num2)
           res = operaciones.mult()
           txtResultado.text = res.toString()

       })

        btnDiv.setOnClickListener(View.OnClickListener {

            var num1 : Float =0.0f
            var num2 : Float =0.0f
            var res  : Float =0.0f

            num1 = txtNum1.text.toString().toFloat()
            num2 = txtNum2.text.toString().toFloat()
            val operaciones  =  Operaciones(num1,num2)
            if(num2!=0.0f) {
                res = operaciones.div()
                txtResultado.text = res.toString()
            }
              else txtResultado.text = "No es posible dividir sobre 0"



        })

    }
}